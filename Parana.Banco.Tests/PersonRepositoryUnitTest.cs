﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using Parana.Banco.API.Controllers;
using Parana.Banco.API.DataEntity;
using Parana.Banco.API.Models;
using Parana.Banco.API.Repository;
using Parana.Banco.API.Repository.Contracts;
using Parana.Banco.API.Services.Contract;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Parana.Banco.Tests
{
    [TestFixture]
    public class PersonRepositoryUnitTest
    {
        public readonly IPersonRepository _repository;

        private readonly Mock<IPersonService> _service;

        private readonly Mock<IMapper> _mapper;

        private readonly Mock<Context> _context;

        //private readonly PersonController _controller;

        public PersonRepositoryUnitTest()
        {
            var _dbSet = new Mock<DbSet<PersonEntity>>();

            this._mapper = new Mock<IMapper>();            
            this._context = new Mock<Context>();
            this._context.Setup(dbc => dbc.Persons).Returns(_dbSet.Object);
            this._repository = new PersonRepository(_context.Object, this._mapper.Object);
        }

        [Test]
        public async Task Test_AddAsync_RepositoryPerson_OK()
        {
            PersonEntity veraLuciaPerson = new PersonEntity
            {
                Id = Guid.NewGuid(),
                Name = "Vera Lucia",
                Email = "vera.lucia@gmail.com",
                PhoneNumber = "41998088874",
                CEP = "82710150",
                City = "Curitiba",
                Street = "Rua Jussara",
                Neighborhood = "Sítio Cercado",
                UF = "PR",
                AditionalInformation = "Casa",
                Number = "3168"
            };
            
            await this._repository.AddAsync(veraLuciaPerson);

            _context.Verify(x => x.Persons, Times.Exactly(1));
            _context.Verify(x => x.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Exactly(1));

        }
        
        [Test]
        public async Task Test_GetAsync_RepositoryPerson_OK()
        {
            PersonModel person = await this._repository.GetByIdAsync(Guid.NewGuid());

            _context.Verify(x => x.FindAsync<PersonEntity>(It.IsAny<PersonEntity>()), Times.Exactly(1));

            Assert.NotNull(person);
        }
    }
}
