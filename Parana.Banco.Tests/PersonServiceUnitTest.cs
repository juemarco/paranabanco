﻿using AutoMapper;
using Moq;
using NUnit.Framework;
using Parana.Banco.API.Models;
using Parana.Banco.API.Repository;
using Parana.Banco.API.Repository.Contracts;
using Parana.Banco.API.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Parana.Banco.Tests
{
    [TestFixture]
    public class PersonServiceUnitTest
    {
        public readonly Mock<PersonRepository> _repository;

        private readonly Mock<IMapper> _mapper;

        public PersonServiceUnitTest()
        {
            this._mapper = new Mock<IMapper>();
            this._repository = new Mock<PersonRepository>(null, this._mapper.Object);
        }

        [Test]
        public async Task SearchPerson_Not_Found()
        {
            _repository.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).Returns(() => Task.FromResult((PersonModel)null));
                        
            var _service = new PersonService(this._mapper.Object, this._repository.Object);
            
            PersonModel model = await _service.GetByIdAsync(Guid.NewGuid());

            Assert.Null(model);
        }

        [Test]
        public async Task SearchPerson_Found()
        {
            _repository.Setup(r => r.GetByIdAsync(It.IsAny<Guid>())).Returns(() => Task.FromResult((PersonModel)null));

            var _service = new PersonService(this._mapper.Object, this._repository.Object);

            PersonModel model = await _service.GetByIdAsync(Guid.NewGuid());

            Assert.IsNotNull(model);
        }

        [Test]
        public async Task SearchPersonAll_Found()
        {
            _repository.Setup(r => r.ListAsync()).Returns(() => Task.FromResult((List<PersonModel>)null));

            var _service = new PersonService(this._mapper.Object, this._repository.Object);

            List<PersonModel> persons = await _service.ListAsync();

            Assert.IsNotEmpty(persons);
        }

        [Test]
        public async Task SearchPersonAll_Not_Found()
        {
            _repository.Setup(r => r.ListAsync()).Returns(() => Task.FromResult((List<PersonModel>)null));

            var _service = new PersonService(this._mapper.Object, this._repository.Object);

            List<PersonModel> persons = await _service.ListAsync();

            Assert.IsEmpty(persons);
        }
    }
}
