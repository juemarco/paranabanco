﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Parana.Banco.API.DataEntity
{
    /// <summary>
    /// Entidade base das entidade gerais
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Identificador unico
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Data de criacao do registro
        /// </summary>
        public DateTime CreationDate { get; private set; }

        /// <summary>
        /// Data de ultima modificacao do registro
        /// </summary>
        public DateTime LastChangeDate { get; private set; }

        /// <summary>
        /// Delecão lógica do 
        /// </summary>
        public bool Active { get; private set; }
    }
}
