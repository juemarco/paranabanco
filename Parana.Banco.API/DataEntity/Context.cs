﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Parana.Banco.API.DataEntity
{
    public class Context : DbContext
    {
        /// <summary>
        /// Construtor padrão do contexto
        /// </summary>
        /// <param name="options"></param>
        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        public Context()
        {
        }

        public virtual DbSet<PersonEntity> Persons { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PersonEntity>();
        }

        /// <summary>
        /// Change efective updates in data base
        /// </summary>
        /// <param name="cancellationToken"> Value of cancellation</param>
        /// <returns> Number of all entities updates</returns>
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            this.ChangeTracker
                .Entries()
                .ToList()
                .ForEach(entry =>
                {
                    if (entry.State == EntityState.Added)
                    {
                        if (entry.Properties.Any(a => a.Metadata.Name.Contains("CreationDate")))
                        {
                            entry.Property("CreationDate").CurrentValue = DateTime.UtcNow;
                        }

                        if (entry.Properties.Any(a => a.Metadata.Name.Contains("Active")))
                        {
                            entry.Property("Active").CurrentValue = true;
                        }
                    }

                    if (entry.Properties.Any(a => a.Metadata.Name.Contains("LastChangeDate")))
                    {
                        entry.Property("LastChangeDate").CurrentValue = DateTime.UtcNow;
                    }
                });

            return (await base.SaveChangesAsync(true, cancellationToken));
        }

    }
}
