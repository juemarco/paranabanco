﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Parana.Banco.API.DataEntity;
using Parana.Banco.API.Mapping;
using Parana.Banco.API.Repository;
using Parana.Banco.API.Repository.Contracts;
using Parana.Banco.API.Services;
using Parana.Banco.API.Services.Contract;
using Parana.Banco.API.Utils;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Parana.Banco.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHttpContextAccessor();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "Paraná Banco API",
                    Version = "v1"
                });
            });

            services.AddScoped<IPersonService, PersonService>();
            services.AddScoped<IPersonRepository, PersonRepository>();

            var connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<Context>
                  (options => options.UseSqlServer(connection));

            AutoMapperConfiguration.Configure();
            services.AddAutoMapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile(Configuration.GetSection("Serilog"));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "cashapi V1");
                c.RoutePrefix = string.Empty;
                c.DocumentTitle = "Paraná Banco Swagger API";
                c.DocExpansion(DocExpansion.None);
            });

            app.UseStaticFiles();
            app.UseCors("ParanaBanco");
            app.UseAuthentication();
            app.ConfigureCustomExceptionMiddleware();
            app.UseMvc();
        }
    }
}
