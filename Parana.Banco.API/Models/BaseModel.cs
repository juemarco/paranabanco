﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parana.Banco.API.Models
{
    /// <summary>
    /// Entidade Base Model Default
    /// </summary>
    public class BaseModel
    {
        private Guid id = Guid.NewGuid();

        /// <summary>
        /// Identificador unico
        /// </summary>

        public Guid Id
        {
            get { return id; }
            private set {  id = value; }
        }

        /// <summary>
        /// Data de criacao do registro
        /// </summary>
        public DateTime CreationDate { get; private set; }

        /// <summary>
        /// Data de ultima modificacao do registro
        /// </summary>
        public DateTime LastChangeDate { get; private set; }

        /// <summary>
        /// Delecão lógica do 
        /// </summary>
        public bool Active { get; private set; }
    }
}
