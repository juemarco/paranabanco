﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Parana.Banco.API.Models
{
    public class PersonModel : BaseModel
    {
        /// <summary>
        /// Nome da Person
        /// </summary>
        [Required(ErrorMessage = "Nome é obrigatório. Min: 2 caractéries. Máx: 100.")]
        [MinLength(2)]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Telefone da Person
        /// </summary>
        [Phone(ErrorMessage = "Número de Telefone Inválido.")]
        [MaxLength(20)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Email da Person
        /// </summary>
        [Required(ErrorMessage = "O E-mail é obrigatório")]
        public string Email { get; set; }

        /// <summary>
        /// Rua do endereço.
        /// </summary>
        [MaxLength(50)]
        public string Street { get; set; }

        /// <summary>
        /// Numero do endereço. Alfa-numérico.
        /// </summary>
        [MaxLength(10)]
        public string Number { get; set; }

        /// <summary>
        /// Bairro do endereço. Alfa-numérico.
        /// </summary>
        [MaxLength(20)]
        public string Neighborhood { get; set; }

        /// <summary>
        /// Complemento. Ex: Casa, Apartamento, Esquina etc.
        /// </summary>
        [MaxLength(20)]
        public string AditionalInformation { get; set; }

        /// <summary>
        /// CEP com 8 caractéries. 
        /// </summary>
        [MaxLength(8)]
        public string CEP { get; set; }

        /// <summary>
        /// Cidade. Alfa-numérico. 
        /// </summary>
        [MaxLength(30)]
        public string City { get; set; }

        /// <summary>
        /// Unidade da Federacao do Brasil. Ex: PR, SC, SP
        /// </summary>
        [MaxLength(2)]
        public string UF { get; set; }
    }
}
