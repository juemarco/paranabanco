﻿using AutoMapper;
using Parana.Banco.API.DataEntity;
using Parana.Banco.API.Models;
using Parana.Banco.API.Repository.Contracts;
using Parana.Banco.API.Services.Contract;

namespace Parana.Banco.API.Services
{
    public class PersonService : BaseService<PersonEntity, PersonModel>, IPersonService
    {
        public PersonService(IMapper mapper, IPersonRepository repository) : base(mapper, repository)
        {
        }
    }
}
