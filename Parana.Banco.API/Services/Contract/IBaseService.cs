﻿using Parana.Banco.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parana.Banco.API.Services.Contract
{
    public interface IBaseService<T> where T : BaseModel
    {
        Task<T> AddAsync(T model);
        Task<T> GetByIdAsync(Guid id);
        Task<List<T>> ListAsync();
        Task UpdateAsync(Guid id, T entity);
        Task DeleteAsync(Guid id);
    }
}
