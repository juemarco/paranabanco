﻿using Parana.Banco.API.Models;

namespace Parana.Banco.API.Services.Contract
{
    public interface IPersonService : IBaseService<PersonModel>
    {
    }
}
