﻿using Parana.Banco.API.DataEntity;
using Parana.Banco.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Parana.Banco.API.Repository.Contracts
{
    public interface IBaseRepository<T1, T2> where T1 : BaseEntity where T2 : BaseModel
    {
        Task<T2> GetByIdAsync(Guid id);
        Task<List<T2>> ListAsync();
        Task<T2> AddAsync(T1 entity);
        Task UpdateAsync(T1 entity);
        Task DeleteAsync(Guid id);
        Task<List<T2>> FindByConditionAsync(Expression<Func<T1, bool>> expression);

    }
}
