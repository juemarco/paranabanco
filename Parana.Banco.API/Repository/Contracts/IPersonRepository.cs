﻿using Parana.Banco.API.DataEntity;
using Parana.Banco.API.Models;

namespace Parana.Banco.API.Repository.Contracts
{
    public interface IPersonRepository : IBaseRepository<PersonEntity, PersonModel>
    {

    }
}
