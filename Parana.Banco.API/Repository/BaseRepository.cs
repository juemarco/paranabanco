﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Parana.Banco.API.DataEntity;
using Parana.Banco.API.Models;
using Parana.Banco.API.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Parana.Banco.API.Repository
{
    /// <summary>
    /// Base Repository Generic
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public class BaseRepository<T1, T2> : IBaseRepository<T1, T2> where T1 : BaseEntity where T2 : BaseModel
    {
        private readonly Context _context;
        private readonly IMapper mapper;

        public BaseRepository(Context context, IMapper mapper)
        {
            this._context = context;
            this.mapper = mapper;
        }

        public Task<T2> GetByIdAsync(Guid id)
        {
            return this._context.Set<T1>().ProjectTo<T2>(mapper).SingleOrDefaultAsync(e => e.Id == id);
        }

        public Task<List<T2>> ListAsync()
        {
            return this._context.Set<T1>().ProjectTo<T2>(mapper).ToListAsync();
        }

        public async Task<T2> AddAsync(T1 entity)
        {
            await this._context.Set<T1>().AddAsync(entity);
            await this._context.SaveChangesAsync();

            return this.mapper.Map<T2>(entity);
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await this._context.Set<T1>().SingleOrDefaultAsync(e => e.Id == id);
            this._context.Set<T1>().Remove(entity);
            await this._context.SaveChangesAsync();
        }

        public async Task UpdateAsync(T1 entity)
        {
            this._context.Entry(entity).State = EntityState.Modified;
            await this._context.SaveChangesAsync();
        }

        public async Task<List<T2>> FindByConditionAsync(Expression<Func<T1, bool>> expression)
        {
            var entities = await this._context.Set<T1>().Where(expression).ToListAsync();
            return this.mapper.Map<List<T2>>(entities);
        }
    }
}
