﻿using AutoMapper;
using Parana.Banco.API.DataEntity;
using Parana.Banco.API.Models;
using Parana.Banco.API.Repository.Contracts;

namespace Parana.Banco.API.Repository
{
    public class PersonRepository : BaseRepository<PersonEntity, PersonModel>, IPersonRepository
    {
        public PersonRepository(Context context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
