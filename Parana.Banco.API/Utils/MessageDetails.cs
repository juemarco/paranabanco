﻿using Newtonsoft.Json;

namespace Parana.Banco.API.Utils
{
    public class MessageDetails
    {
        public MessageDetails(MessageTypeEnum type, string message)
        {
            Type = type;
            Message = message;
        }

        public MessageTypeEnum Type { get; set; }

        public string Message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
