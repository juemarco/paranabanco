﻿using Microsoft.EntityFrameworkCore;
using Parana.Banco.API.DataEntity;
using System;
using System.Threading.Tasks;

namespace Parana.Banco.API.Utils
{

    /// <summary>
    /// Seed do banco de dados
    /// </summary>
    public class SeedData
    {
        public static async Task SeedAsync(Context context)
        {
            await context.Database.MigrateAsync();

            if (!await context.Persons.AnyAsync())
            {
                PersonEntity marcoGabrielPerson = new PersonEntity
                {
                    Id = Guid.NewGuid(),
                    Name = "Marco Gabriel",
                    Email = "gabriel@marcogabriel.com.br",
                    PhoneNumber = "41998090323",
                    CEP = "83404510",
                    City = "Curitiba",
                    Street = "Travessa Montevideo",
                    Neighborhood = "Campo Pequeno",
                    UF = "PR",
                    AditionalInformation = "Apartamento",
                    Number = "58"
                };

                PersonEntity veraLuciaPerson = new PersonEntity
                {
                    Id = Guid.NewGuid(),
                    Name = "Vera Lucia",
                    Email = "vera.lucia@gmail.com",
                    PhoneNumber = "41998088874",
                    CEP = "82710150",
                    City = "Curitiba",
                    Street = "Rua Jussara",
                    Neighborhood = "Sítio Cercado",
                    UF = "PR",
                    AditionalInformation = "Casa",
                    Number = "3168"
                };

                context.Persons.Add(marcoGabrielPerson);
                context.Persons.Add(veraLuciaPerson);
                await context.SaveChangesAsync();
            }           
        }
    }
}
