﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parana.Banco.API.Utils
{
    public class CustomException : Exception
    {
        public CustomException(MessageDetails message)
        : base(message.ToString())
        {
        }

        public CustomException(MessageDetails message, Exception inner)
         : base(message.ToString(), inner)
        {
        }
    }
}
