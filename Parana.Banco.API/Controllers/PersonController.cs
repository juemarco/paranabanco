﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Parana.Banco.API.Models;
using Parana.Banco.API.Services.Contract;

namespace Parana.Banco.API.Controllers
{
    /// <summary>
    /// Controller responsável pelas ações da entidade Person
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : BaseController<PersonModel>
    {
        public PersonController(IPersonService service) : base(service)
        {

        }
    }
}
