﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Parana.Banco.API.Models;
using Parana.Banco.API.Services.Contract;
using Parana.Banco.API.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Parana.Banco.API.Controllers
{
    public class BaseController<T> : ControllerBase where T : BaseModel
    {
        private readonly IBaseService<T> _service;

        public BaseController(IBaseService<T> service)
        {
            this._service = service;
        }

        [HttpGet("")]
        public virtual async Task<ActionResult<List<T>>> GetAsync()
        {
            return await this._service.ListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<T>> GetAsync(Guid id)
        {
            return await this._service.GetByIdAsync(id);
        }

        [HttpPost]
        public async Task<ActionResult<T>> PostAsync([FromBody]T item)
        {
            if (this.ModelState.IsValid)
            {
                item = await this._service.AddAsync(item);
                return Created(nameof(item), new { id = item.Id });
            }

            throw new CustomException(new MessageDetails(MessageTypeEnum.Error, MessagesErrorsModel(this.ModelState)));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(Guid id, T item)
        {
            if (this.ModelState.IsValid)
            {
                await this._service.UpdateAsync(id, item);
                return NoContent();
            }

            throw new CustomException(new MessageDetails(MessageTypeEnum.Error, MessagesErrorsModel(this.ModelState)));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            await this._service.DeleteAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Execute get errors model
        /// </summary>
        protected static string MessagesErrorsModel(ModelStateDictionary modelstate)
        {
            if (modelstate is null)
            {
                throw new CustomException(new MessageDetails(MessageTypeEnum.Error, $"Desculpe, mas o objeto passado é inválido"));
            }

            string messages = string.Join("; ", modelstate.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
            return messages;
        }
    }
}
