﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Parana.Banco.API.Migrations
{
    public partial class IntialDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    LastChangeDate = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: true),
                    Email = table.Column<string>(maxLength: 30, nullable: true),
                    Street = table.Column<string>(maxLength: 50, nullable: true),
                    Number = table.Column<string>(maxLength: 10, nullable: true),
                    Neighborhood = table.Column<string>(maxLength: 20, nullable: true),
                    AditionalInformation = table.Column<string>(maxLength: 20, nullable: true),
                    CEP = table.Column<string>(maxLength: 8, nullable: true),
                    City = table.Column<string>(maxLength: 30, nullable: true),
                    UF = table.Column<string>(maxLength: 2, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Persons");
        }
    }
}
