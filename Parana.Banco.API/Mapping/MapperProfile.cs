﻿using AutoMapper;
using Parana.Banco.API.DataEntity;
using Parana.Banco.API.Models;

namespace Parana.Banco.API.Mapping
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            this.CreateMap<BaseEntity, BaseModel>();
            this.CreateMap<BaseModel, BaseEntity>();

            this.CreateMap<PersonEntity, PersonModel>();
            this.CreateMap<PersonModel, PersonEntity>();
        }
    }
}
